/**
* @file stack.h
* @author Adam Olofsson <adol1601@student.miun.se>
* @version 1.1
*/

#ifndef _STACK_H_
#define _STACK_H_

#include <stdexcept>
#include <algorithm>

/**
* @class Node
* @brief Class for a node
* @details Each node contains data and an adress to the next node (if there are any)
*/

template <typename T>
class Node {
public:
    Node<T>* next;
    T data;
    /**
    *Constructor that initializes a node
    *
    * @param pnode a pointer to the next node (if there are any)
    * @param pdata a value of type T (template<typename>)
    *
    * @code Node(nextNode, "habbakukk"): next(pnode), data(pdata) @endcode
    */

    Node(Node<T>* pnode = nullptr, T pdata = 0): next(pnode), data(pdata) {}
};

/**
* @class Stack
* @brief Abstract datatype stack for storing data.
* @details Uses the @ref Node class to create a linked list of data entities following the principle of LIFO(last in first out).
*/

template <typename T>
class Stack {
private:
    Node<T>* top = nullptr;
    int _size = 0;
public:
    Stack() {}
    ~Stack();
    void push (T data);

    T pop();
/**
* Method to get the Stack size
*
* @return size of stack (int)
*/
    int size() { return _size; }

/**
* Method to find out if Stack is empty or not
*
* @return bool - true if empty
*/
    bool empty() {return _size == 0;}
};

/**
* Destructor for Stack
*
* Makes sure that the memory is deallocated
*
* ####Example:
* @code ~aStack(); @endcode
*/
template <typename T>
Stack<T>::~Stack() {
  while(!empty()) {
    Node<T> *oldTop = top;
    top = top->next;
    delete oldTop;
    _size--;
  }
}

/**
* Method for removing the top of the Stack.
*
* @return T - data of the top element.
*
* @code aStack.pop(); @endcode
*/
template <typename T>
T Stack<T>::pop() {
  if (empty()) {
    throw std::range_error{"Can't pop empty stack"};
  } else {
    T topData = top->data;
    Node<T> *oldTop = top;
    top = top->next;
    delete oldTop;
    _size--;
    return topData;
  }
}

/**
* Method for adding data to the Stack
*
* @param data type T data (typename)
*
* ####Examples:
*
* @code aStack.push(8) @endcode
* @code aStack.push("Helloworld") @endcode
*/
template <typename T>
void Stack<T>::push(T data) {
  Node<T>* newTop = new Node<T>(top,data);
  top = newTop;
  _size++;
}



#endif
